# terraform

## Instruções

1. Abra o prompt de sua preferência (MS-DOS, Windows PowerShell, Linux bash, etc)
2. Certifique-se se o docker está rodando na máquina

```
 docker info
```

3. Conecte no Container Registry do Gitlab

```
docker login registry.gitlab.com
```

4. Acesse o local do repositório e faça a construção da imagem

```
cd git\terraform_gitflow\docker\images\terraform\
docker build -t registry.gitlab.com/terraform_gitflow/docker/images/terraform .
```

5. Faça um teste da imagem construída

```
docker run registry.gitlab.com/terraform_gitflow/docker/images/terraform terraform -version
```

6. Crie uma tag para a imagem

```
docker tag registry.gitlab.com/terraform_gitflow/docker/images/terraform registry.gitlab.com/terraform_gitflow/docker/images/terraform:13.3
```
7. Faça o envio da imagem para o Container Registry do Gitlab

```
docker push registry.gitlab.com/terraform_gitflow/docker/images/terraform:13.3
```