FROM alpine

ARG TERRAFORM_VERSION
ARG TERRAFORM_ZIP_FILE=terraform_0.${TERRAFORM_VERSION}_linux_amd64.zip

COPY ${TERRAFORM_ZIP_FILE} /tmp

RUN unzip /tmp/${TERRAFORM_ZIP_FILE} -d /usr/bin && \
    rm -rf /tmp/${TERRAFORM_ZIP_FILE} && \
    chmod +x /usr/bin/terraform